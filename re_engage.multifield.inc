<?php
/**
 * @file
 * re_engage.multifield.inc
 */

/**
 * Implements hook_multifield_default_multifield().
 */
function re_engage_multifield_default_multifield() {
  $export = array();

  $multifield = new stdClass();
  $multifield->disabled = FALSE; /* Edit this to true to make a default multifield disabled initially */
  $multifield->api_version = 1;
  $multifield->machine_name = 'campaign_target';
  $multifield->label = 'Campaign Target';
  $multifield->description = '';
  $export['campaign_target'] = $multifield;

  return $export;
}
