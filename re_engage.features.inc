<?php
/**
 * @file
 * re_engage.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function re_engage_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "multifield" && $api == "multifield") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function re_engage_node_info() {
  $items = array(
    'campaign' => array(
      'name' => t('Campaign'),
      'base' => 'node_content',
      'description' => t('Use <em>campaigns</em> to organize data about one of PSAC\'s specific goals or gather signatures in a petition.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implements hook_default_registration_type().
 */
function re_engage_default_registration_type() {
  $items = array();
  $items['campaign_registration'] = entity_import('registration_type', '{
    "name" : "campaign_registration",
    "label" : "Campaign Registration",
    "locked" : "0",
    "weight" : "0",
    "data" : null,
    "rdf_mapping" : []
  }');
  return $items;
}
