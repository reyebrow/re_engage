<?php
/**
 * @file
 * re_engage.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function re_engage_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create campaign_registration registration'.
  $permissions['create campaign_registration registration'] = array(
    'name' => 'create campaign_registration registration',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'create campaign_registration registration other anonymous'.
  $permissions['create campaign_registration registration other anonymous'] = array(
    'name' => 'create campaign_registration registration other anonymous',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'registration',
  );

  return $permissions;
}
