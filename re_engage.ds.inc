<?php
/**
 * @file
 * re_engage.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function re_engage_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|campaign|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'campaign';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'field_highlighted_image' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'left',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
        ),
      ),
    ),
    'field_sign_our_petition' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
  );
  $export['node|campaign|full'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function re_engage_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|campaign|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'campaign';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'zf_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_highlighted_image',
        1 => 'body',
        3 => 'field_sign_our_petition',
      ),
      'ds_hidden' => array(
        2 => 'field_target',
      ),
    ),
    'fields' => array(
      'field_highlighted_image' => 'ds_content',
      'body' => 'ds_content',
      'field_target' => 'ds_hidden',
      'field_sign_our_petition' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
      'ds_hidden' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|campaign|full'] = $ds_layout;

  return $export;
}
